<?php

$dest = "uploads/";

if (!is_dir($dest)) {
    mkdir($dest, 0777);
}

foreach ($_FILES["images"]["error"] as $key => $error) {
  if ($error == UPLOAD_ERR_OK) {
    $name = $_FILES["images"]["name"][$key];
    // move_uploaded_file( $_FILES["images"]["tmp_name"][$key], $dest . $_FILES['images']['name'][$key]);
    $ext = strrchr($_FILES['images']['name'][$key], ".");
    move_uploaded_file( $_FILES["images"]["tmp_name"][$key], $dest . $_POST['name'] . "_" . $key . $ext);
  }
}

echo "<h2>Successfully Uploaded Images</h2>";