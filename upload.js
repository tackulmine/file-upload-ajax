jQuery.noConflict();
(function($) {

    $.fn.serializeFiles = function() {
        var form = $(this),
            formData = new FormData(),
            formParams = form.serializeArray();

        $.each(form.find('input[type="file"]'), function(i, tag) {
            $.each($(tag)[0].files, function(i, file) {
                formData.append(tag.name, file);
            });
        });

        $.each(formParams, function(i, val) {
            formData.append(val.name, val.value);
        });

        return formData;
    };

    $(function() {

        var input = document.getElementById("images");
        // var input = $("#images"),
        // formData = false;

        if (window.FormData) {
            // formData = new FormData();
            // document.getElementById("btn").style.display = "none";
            $("#btn").prop('disabled', true);
        }

        function showUploadedItem(source) {
            // var list = document.getElementById("image-list"),
            var list = $("#image-list"),
                li = document.createElement("li"),
                img = document.createElement("img");
            img.src = source;
            li.appendChild(img);
            // list.appendChild(li);
            list.append(li);
        }

        if (input.addEventListener) {
            input.addEventListener("change", function(evt) {
                var i = 0,
                    len = this.files.length,
                    img, reader, file;

                // document.getElementById("response").innerHTML = "Uploading . . .";
                // $("#response").html("Uploading . . .");

                for (; i < len; i++) {
                    file = this.files[i];

                    if (!!file.type.match(/image.*/)) {

                        if (window.FileReader) {
                            reader = new FileReader();
                            reader.onloadend = function(e) {
                                showUploadedItem(e.target.result);
                            };
                            reader.readAsDataURL(file);
                        }
                        // if (formData) {
                        //     formData.append("images[]", file);
                        // }

                        $("#btn").prop('disabled', false);

                        // if (formData) {
                        //     $.ajax({
                        //         url: "upload.php",
                        //         type: "POST",
                        //         data: formData,
                        //         processData: false,
                        //         contentType: false,
                        //         success: function(res) {
                        //             // document.getElementById("response").innerHTML = res;
                        //             $("#response").html(res);
                        //         }
                        //     });
                        // }

                    }
                }

            }, false);
        }

        $(document).on('submit', '#form', function(e) {

            e.preventDefault();

            var formParams = $(this).serializeFiles();
            // if (formData) {
            // formData.append('name_file', $('[name="name"]').val());
            // console.log(formData);
            if (formParams) {
                console.log(formParams);

                $.ajax({
                    url: "upload.php",
                    type: "POST",
                    data: formParams,
                    processData: false,
                    contentType: false,
                    success: function(res) {
                        // document.getElementById("response").innerHTML = res;
                        $("#response").html(res);
                    }
                });
            }

        });

    });


})(jQuery);